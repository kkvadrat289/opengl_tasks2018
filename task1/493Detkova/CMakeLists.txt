set(SRC_FILES
    Main.cpp
    Maze.cpp
    Maze.hpp
    MazeCameraMover.hpp
    MazeCameraMover.cpp
    common/Application.cpp
    common/DebugOutput.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
)

set(HEADER_FILES
    common/Application.hpp
    common/DebugOutput.h
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
    
)

MAKE_TASK(493Detkova 1 "${SRC_FILES}")
