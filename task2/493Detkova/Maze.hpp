#pragma once
#include <memory>
#include <vector>
#include <random>
#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

class Cell{
public:
    Cell(const int ind);
    void CalculateNeighbours(const int size);
    void AddWall(const int to);
    int GetNumOfOpenedNeighbours() const;
    const std::vector<int> GetNeighbours() const;
    int GetNumOfNeighbours() const;
    int GetIndex() const;
private:
    int index;
    std::vector<int> neighbours; 
    std::vector<int> openedNeighbours;
};

class Maze{
public:
    Maze(const int numOfCells, const float sizeOfCell);
    void GenerateStructure();
    MeshPtr makeMesh() const;
    MeshPtr makeFloor() const;
    MeshPtr makeCeil() const;
    std::vector<MeshPtr> makePosters() const;
    void PrintStructure() const;
    void AddWallToMesh(std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2> &texcoords, const int first, const int second) const;
    float GetCellSize( )const{return cellSize;}
    int GetCellNum() const{return cellNum;}
    float GetHeight() const{return height;}
    std::vector<int> GetBlockedCells(const int cell) const;
private:
    void addPoster(std::vector<MeshPtr>& posters, const std::pair<int, int> wall) const;
    float height;
    bool checkConnectivity() const;
    int cellNum;
    float cellSize;
    std::vector<std::shared_ptr<Cell> > cells;
    std::vector<std::pair<int, int> > walls;
};

int findCell(const std::vector<int> &vec, int cell);
int findWall(const std::vector<std::pair<int, int> > &walls, const int first, const int second);
