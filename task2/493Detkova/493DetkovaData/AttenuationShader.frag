/**
Пофрагментное освещение точечным источником света с учетом затухания. Окружающий, диффузный и бликовый свет.
*/

#version 330

struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
    float attenuation; //затухание
    int onLight;
//    vec3 direction;
};

uniform LightInfo light;

uniform LightInfo observeLight;

struct MaterialInfo
{
    vec3 Ka; //коэффициент отражения окружающего света
    vec3 Kd; //коэффициент отражения диффузного света
    vec3 Ks; //коэффициент отражения бликового света
    float shininess;
};
uniform MaterialInfo material;

uniform float angle;

uniform sampler2D diffuseTex;
uniform sampler2D normalTex;
uniform vec3 lightDirection;
uniform mat3 normalToCameraMatrix;

in vec2 texCoord;
in vec3 normalTextureCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 lightPosCamSpace; //положение источника света в системе координат камеры (интерполировано между вершинами треугольника)
in vec4 observeLightPosCamSpace;
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 outputNormal;
in vec3 outputTangent;
in vec3 outputBitangent;


in vec3 cameraLightDirection;

out vec4 fragColor; //выходной цвет фрагмента

void main()
{
    vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
    vec3 viewDirection = normalize(-posCamSpace.xyz);

//    vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
    vec3 normalTangentSpace;

    vec3 normalCamSpace;
    float cosAngle = cos(angle);

        mat3 deformationMatrix = mat3(
            outputTangent, outputBitangent, outputNormal
        );

        normalTangentSpace = normalize(texture(normalTex, texCoord).rgb * 2.0 - 1.0);
        normalCamSpace = normalize(normalToCameraMatrix * deformationMatrix * normalTangentSpace);
    


     //скалярное произведение (косинус)

    vec3 color = vec3(0.0);
    if (light.onLight > 0.1) {
        vec3 lightDirCamSpace = lightPosCamSpace.xyz - posCamSpace.xyz; //направление на источник света
        float distance = length(lightDirCamSpace);
        lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

        float attenuationCoef = 1.0 / (1.0 + light.attenuation * distance);

        float NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0);

        float newCos = dot(normalize(lightDirCamSpace.xyz), cameraLightDirection);
        float fDif = 1.0 - cosAngle;
        float visibilityLevel = clamp((newCos - cosAngle) / fDif, 0.0, 1.0);
        if (newCos < cosAngle) {
            visibilityLevel = 0.0;
        }
        color += diffuseColor * (light.La + light.Ld * NdotL * attenuationCoef * visibilityLevel); //цвет вершины



        if (NdotL > 0.0) {
             //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
            vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

            float blinnTerm = max(dot(normalCamSpace, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, material.shininess); //регулируем размер блика

            color += light.Ls * material.Ks * blinnTerm * attenuationCoef * visibilityLevel;
        }
    }

    if (observeLight.onLight > 0.1) {
        vec3 lightDirCamSpace = normalize(observeLightPosCamSpace.xyz - posCamSpace.xyz);
        float NdotL = max(dot(normalCamSpace, lightDirCamSpace.xyz), 0.0);
        color += diffuseColor * (observeLight.La + observeLight.Ld * NdotL); //цвет вершины


        if (NdotL > 0.0) {

            vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

            float blinnTerm = max(dot(normalCamSpace, halfVector), 0.0); //интенсивность бликового освещения по Блинну
            blinnTerm = pow(blinnTerm, material.shininess); //регулируем размер блика

            color += observeLight.Ls * material.Ks * blinnTerm;
        }
    }



    fragColor = vec4(color, 1.0); //просто копируем
}
