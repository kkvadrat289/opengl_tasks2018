/**
Пофрагментное освещение точечным источником света с учетом затухания. Окружающий, диффузный и бликовый свет.
*/

#version 330

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 normalToCameraMatrix;
uniform int isPoster; // загружается ли текстура в шейдер

struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    vec3 La; //цвет и интенсивность переотраженного света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
    float attenuation; //затухание
    int onLight;
//    vec3 direction;
};
uniform LightInfo light;
uniform LightInfo observeLight;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec3 vertexTangent;
layout(location = 4) in vec3 vertexBitangent;


out vec4 lightPosCamSpace; //положение источника света в системе координат камеры
out vec4 observeLightPosCamSpace; // положение источника внешнего света в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord;

out vec3 normalTextureCamSpace; //нормаль в системе координат камеры для текстур

uniform float angle;
uniform vec3 lightDirection;
out vec3 cameraLightDirection;
out vec3 outputNormal;
out vec3 outputTangent;
out vec3 outputBitangent;

void main()
{
    texCoord = vertexTexCoord;
    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
    if (isPoster > 0) {
        normalTextureCamSpace = normalize(normalToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
    }
    lightPosCamSpace = viewMatrix * vec4(light.pos, 1.0); //преобразование положения источника света в систему координат камеры
    observeLightPosCamSpace = viewMatrix * vec4(observeLight.pos, 1.0);
    cameraLightDirection = normalize((viewMatrix * vec4(lightDirection, 0.0)).xyz);
    outputNormal = vertexNormal;

    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}