#include "MazeCameraMover.hpp"

#include <glm/gtx/transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>
#include <cmath>

MazeCameraMover::MazeCameraMover(const std::shared_ptr<Maze> maze) :
        CameraMover(),
        _oldXPos(0),
        _oldYPos(0),
        _pos(maze->GetCellSize() * maze->GetCellNum() - maze->GetCellSize()/2 ,
             maze->GetCellSize() * maze->GetCellNum() - maze->GetCellSize()/2 ,
             -maze->GetHeight()/2)
{
    // Зададим начальную ориентацию камеры
  //  std::cout << _pos.x << std::endl << _pos.y << std::endl;
    _maze = maze;
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.01f, -maze->GetHeight()/2), glm::vec3(0.0f, 0.0f, 1.0f)));

}

void MazeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MazeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;
    //Получаем текущее направление "вперед" в мировой системе координат

    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    float _oldposZ = _pos.z;
    float _prevPosX = _pos.x;
    float _prevPosY = _pos.y;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        _pos += rightDir * speed * static_cast<float>(dt);
    }



    _pos.z = _oldposZ; // Фиксируем положение по оси OZ, чтобы не вылетало за границу

    if (CheckCollision(glm::vec2(_pos.x, _pos.y))) {
        _pos.x = _prevPosX;
        _pos.y = _prevPosY;
    }

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

bool MazeCameraMover::CheckCollision(glm::vec2 position) {
    float size = _maze->GetCellNum() * _maze->GetCellSize();
    if (position.x < _maze->GetCellSize()/9 || position.x > size - _maze->GetCellSize()/9||
        position.y > size - _maze->GetCellSize()/9|| position.y < _maze->GetCellSize()/9){
        return true;
    }
    if (position.x > floorf(position.x) - _maze->GetCellSize()/5-0.1f && position.x < floorf(position.x)+ _maze->GetCellSize()/5+0.1f &&
        position.y > floorf(position.y) - _maze->GetCellSize()/5-0.1f && position.y < floorf(position.y)+ _maze->GetCellSize()/5+0.1f){
        return true;
    }

    float lineX = floorf(position.x);
    float rowY = floorf(position.y);
    int currentCellNumber = lineX * _maze->GetCellNum() + rowY;
    std::vector<int> blockedCells = _maze->GetBlockedCells(currentCellNumber);

    for (auto nextCell : blockedCells){
        //переход справа налево
        if (currentCellNumber - nextCell == 1){
            if (position.y < std::max(nextCell, currentCellNumber) % _maze->GetCellNum() * _maze->GetCellSize() + _maze->GetCellSize()/5+0.1f){
                return true;
            }
        }
        //переход справа налево
        else if (currentCellNumber - nextCell == -1){
            if (position.y > std::max(nextCell, currentCellNumber) % _maze->GetCellNum() * _maze->GetCellSize() - _maze->GetCellSize()/5-0.1f){
                return true;
            }
        }
        //переход снизу вверх
        else if (currentCellNumber - nextCell == _maze->GetCellNum()){
            if (position.x < std::max(nextCell, currentCellNumber) / _maze->GetCellNum() * _maze->GetCellSize() + _maze->GetCellSize()/5+0.1f){
                return true;
            }
        }
        //переход сверху вниз
        else{
            if (position.x > std::max(nextCell, currentCellNumber) / _maze->GetCellNum() * _maze->GetCellSize() - _maze->GetCellSize()/5-0.1f){
                return true;
            }
        }

    }

    return false;
}

glm::vec3 MazeCameraMover::getPosition() {
    return _pos;
}

glm::vec3 MazeCameraMover::getDirection() {
    glm::vec3 ans = glm::normalize(glm::vec3(0.0f, 0.0f, 1.0f) * _rot);
    return ans;

}
