#include "Maze.hpp"
#include <iostream>
#include <queue>

Maze::Maze(const int numOfCells, const float sizeOfCell): height(2*sizeOfCell), cellNum(numOfCells), cellSize(sizeOfCell){

}

Cell::Cell(const int ind): index(ind){
    
}

int Cell::GetIndex() const{
    return index;
}

void Cell::CalculateNeighbours(const int size){
    //up
    if (index >= size){
        neighbours.push_back(index - size);
    }
    //right
    if (index % size != size - 1){
        neighbours.push_back(index + 1);
    }
    //down
    if (index < size * (size - 1)){
        neighbours.push_back(index + size);
    }
    //left
    if (index % size != 0){
        neighbours.push_back(index - 1);
    }
}

const std::vector<int> Cell::GetNeighbours() const{
    return neighbours;
}

void Cell::AddWall(const int to){
    openedNeighbours.push_back(to);
}

int Cell::GetNumOfOpenedNeighbours() const{
    return openedNeighbours.size();
}

void Maze::GenerateStructure(){
    for (int i = 0; i < cellNum * cellNum; i++){
        cells.push_back(std::make_shared<Cell> (i));
        cells.back()->CalculateNeighbours(cellNum);
    }
    for (auto cell : cells){
        for (auto neighbour : cell->GetNeighbours()){
            if (neighbour > cell->GetIndex()){
                walls.push_back(std::make_pair(cell->GetIndex(), neighbour));
            }
        }
    }

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis (0, cellNum * cellNum);
    std::vector<int> opened;
    std::vector<int> border;
    opened.push_back(0);
    border.push_back(0);
    while (opened.size() < cellNum * cellNum){
        int currentCell = border[dis(gen) % border.size()];
        int indexToDeleteWall = dis(gen) % cells[currentCell]->GetNumOfNeighbours();
        int nextCell = cells[currentCell]->GetNeighbours()[indexToDeleteWall];
        if (findCell(opened, nextCell) == -1){
            int first = std::min(currentCell, nextCell);
            int second = std::max(currentCell, nextCell);
            walls.erase(walls.begin() + findWall(walls, first, second));
            opened.push_back(nextCell);
            if (cells[currentCell]->GetNumOfNeighbours() == cells[currentCell]->GetNumOfOpenedNeighbours()){
                border.erase(border.begin() + findCell(border, currentCell));
            }
            if (cells[nextCell]->GetNumOfNeighbours() != cells[nextCell]->GetNumOfOpenedNeighbours()){
                border.push_back(nextCell);
            }
        }
    }
}

int findCell(const std::vector<int> &vec, int cell){
    for (int i = 0; i < vec.size(); i++){
        if (vec[i] == cell){
            return i;
        }
    }
    return -1;
}

int findWall(const std::vector<std::pair<int, int> > &walls, const int first, const int second){
    for (int i = 0; i < walls.size(); i++){
        if (walls[i].first == first && walls[i].second == second){
            return i;
        }
    }
    return -1;
}

int Cell::GetNumOfNeighbours() const{
    return neighbours.size();
}

MeshPtr Maze::makeCeil() const{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float size = cellSize * cellNum / 2 + cellSize/9.9;
    //ceil
    vertices.push_back(glm::vec3(-size, size, 0.001));
    vertices.push_back(glm::vec3(size, size, 0.001));
    vertices.push_back(glm::vec3(size, -size, 0.001));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), static_cast<float>(cellNum)));
    texcoords.push_back(glm::vec2(0.0, static_cast<float>(cellNum)));

    //ceil
    vertices.push_back(glm::vec3(-size, size, 0.001));
    vertices.push_back(glm::vec3(size, -size, 0.001));
    vertices.push_back(glm::vec3(-size, -size, 0.001));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(0.0, static_cast<float>(cellNum)));

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr Maze::makeFloor() const{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    float size = cellSize * cellNum / 2 + cellSize/9.9;
    //floor
    vertices.push_back(glm::vec3(size, -size, -height));
    vertices.push_back(glm::vec3(size, size, -height));
    vertices.push_back(glm::vec3(-size, size, -height));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(10.0, 0.0));
    texcoords.push_back(glm::vec2(10.0, 10.0));
    texcoords.push_back(glm::vec2(0.0, 10.0));

    //floor2
    vertices.push_back(glm::vec3(-size, -size, -height));
    vertices.push_back(glm::vec3(size, -size, -height));
    vertices.push_back(glm::vec3(-size, size, -height));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(10.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 10.0));

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

MeshPtr Maze::makeMesh() const{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    //float height = cellSize * cellNum / 2;
    float size = cellSize * cellNum / 2 + cellSize/9.9;

    //outher walls
    //back
    vertices.push_back(glm::vec3(-size, size, -height));
    vertices.push_back(glm::vec3(-size, size, 0));
    vertices.push_back(glm::vec3(-size, -size + cellSize, 0));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 1.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));


    vertices.push_back(glm::vec3(-size, -size + cellSize, -height));
    vertices.push_back(glm::vec3(-size, size, -height));
    vertices.push_back(glm::vec3(-size, -size + cellSize, 0));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));

    //right
    vertices.push_back(glm::vec3(-size, size, 0));
    vertices.push_back(glm::vec3(-size, size, -height));
    vertices.push_back(glm::vec3(size, size, 0));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 1.0));


    vertices.push_back(glm::vec3(-size, size, -height));
    vertices.push_back(glm::vec3(size, size, -height));
    vertices.push_back(glm::vec3(size, size, 0));

    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));
    normals.push_back(glm::vec3(0.0, -1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 1.0));

    //left
    vertices.push_back(glm::vec3(-size, -size, -height));
    vertices.push_back(glm::vec3(-size, -size, 0));
    vertices.push_back(glm::vec3(size, -size, 0));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 1.0));


    vertices.push_back(glm::vec3(size, -size, -height));
    vertices.push_back(glm::vec3(-size, -size, -height));
    vertices.push_back(glm::vec3(size, -size, 0));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 1.0));


    //front
    vertices.push_back(glm::vec3(size, size, -height));
    vertices.push_back(glm::vec3(size, -size, -height));
    vertices.push_back(glm::vec3(size, -size, 0));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));

    vertices.push_back(glm::vec3(size, size, 0));
    vertices.push_back(glm::vec3(size, size, -height));
    vertices.push_back(glm::vec3(size, -size, 0));

    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

    texcoords.push_back(glm::vec2(static_cast<float>(cellNum),1.0));
    texcoords.push_back(glm::vec2(static_cast<float>(cellNum), 0.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));


    //inner walls
    for (auto wall : walls){
        //if (wall.first == 1 || wall.first == 0)
            AddWallToMesh(vertices, normals, texcoords, wall.first, wall.second);
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

std::vector<MeshPtr> Maze::makePosters() const{
    std::vector<MeshPtr> posters;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis (0, walls.size());

    const int numOfPosters = 9;

    for (int i = 0; i < numOfPosters; i++){
        int wallWithPosterIndex = dis(gen);
        addPoster(posters, walls[wallWithPosterIndex]);
    }
    return posters;
}

void Maze::addPoster(std::vector<MeshPtr>& posters, const std::pair<int, int> wall) const{

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;
    int first = wall.first;
    int second = wall.second;
    //int first = 1;
    //int second = 2;
    float x1, x2, y1, y2;

    float size = cellSize * cellNum / 2;
    float eps = cellSize / 9.8;
    if (second - first == 1){
        std::cout << "плакат между клетками " << first << "и " << second << std::endl;
        //стена разделяет две клетки по горизонтали
        y1 = cellSize * (second % cellNum) - size;
        x1 = cellSize * (first / cellNum) - size;
        x2 = x1 + cellSize;
        y2 = y1;

        vertices.push_back(glm::vec3(x1, y1 + eps, -height + cellSize/2));
        vertices.push_back(glm::vec3(x1, y1 + eps, -cellSize/3));
        vertices.push_back(glm::vec3(x2, y1 + eps, -cellSize/3));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));

        //left 2
        vertices.push_back(glm::vec3(x1, y1 + eps, -height+ cellSize/2));
        vertices.push_back(glm::vec3(x2, y1 + eps, -cellSize/3));
        vertices.push_back(glm::vec3(x2, y1 + eps, -height+ cellSize/2));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(1.0, 0.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
    }
    else{
        //стена разделяет две клетки по вертикали
        std::cout << "Плакат между клетками " << first << "и " << second << std::endl;
        y1 = cellSize * (first % cellNum) - size;
        x1 = cellSize * (1 + first / cellNum) - size;
        y2 = y1 + cellSize;
        x2 = x1;

        //front 1
        vertices.push_back(glm::vec3(x1 - eps, y2, -cellSize/3));
        vertices.push_back(glm::vec3(x1 - eps, y2, -height + cellSize/2));
        vertices.push_back(glm::vec3(x1 - eps, y1, -height + cellSize/2));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));


        //front 2
        vertices.push_back(glm::vec3(x1 - eps, y1, -cellSize/3));
        vertices.push_back(glm::vec3(x1 - eps, y2, -cellSize/3));
        vertices.push_back(glm::vec3(x1 - eps, y1, -height + cellSize/2));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(1.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(1.0, 0.0));
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    posters.push_back(mesh);
}

void Maze::PrintStructure() const{
    std::cout << "Number of cells: " << cellNum * cellNum << std::endl;
    std::cout << "Size of cell: " << cellSize <<std::endl;
    std::cout << walls.size() <<' '<<walls.size() <<std::endl;

    for (auto wall: walls){
        std::cout << wall.first << '|' << wall.second;
    }
}


void Maze::AddWallToMesh(std::vector<glm::vec3> &vertices, std::vector<glm::vec3> &normals, std::vector<glm::vec2> &texcoords, const int first, const int second) const{
    float x1;
    float y1;
    float x2;
    float y2;

    float eps = cellSize/10;

    float size = cellSize * cellNum / 2;

    float width = 2*eps;
    float length = 2*eps + cellSize;

    //horizontal cells
    if (second - first == 1){
        y1 = cellSize * (second % cellNum) - size;
        x1 = cellSize * (first / cellNum) - size;
        x2 = x1 + cellSize;
        y2 = y1;

        //front 1
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, 0));

        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(width, 1.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));

        //front 2
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));

        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(width, 0.0));

        //left 1
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, 0));

        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(length, 0.0));
        texcoords.push_back(glm::vec2(length, 1.0));

        //left 2
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, -height));

        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(length, 0.0));


       //top 1
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(0.0, width));
        texcoords.push_back(glm::vec2(length, 0.0));
        texcoords.push_back(glm::vec2(length, width));


        //top 2
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 - eps, 0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(0.0, width));
        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(length, 0.0));

        //back 1
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, -height));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(width, 1.0));
        texcoords.push_back(glm::vec2(width, 0.0));


        //back 2
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, -height));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, -height));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));


        //right 1
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(length, 1.0));
        texcoords.push_back(glm::vec2(length, 0.0));


        //right 2
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));
        vertices.push_back(glm::vec3(x1 - eps, y1 + eps, -height));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(length, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
    }

    //vertical cells
    else{
        y1 = cellSize * (first % cellNum) - size;
        x1 = cellSize * (1 + first / cellNum) - size;
        y2 = y1 + cellSize;
        x2 = x1;

        //front 1
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));

        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(length, 0.0));
        texcoords.push_back(glm::vec2(length, 1.0));


        //front 2
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, -height));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, 0));

        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 0.0));
        texcoords.push_back(glm::vec2(length, 1.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));

      //left 1
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, -height));
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));

        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));

        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(width, 1.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));

        //left 2
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, -height));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, -height));

        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));
        normals.push_back(glm::vec3(0.0, -1.0, 0.0));

        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));

       //top 1
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, 0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(width, length));
        texcoords.push_back(glm::vec2(0.0, length));


        //top 2
        vertices.push_back(glm::vec3(x1 + eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));

        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));
        normals.push_back(glm::vec3(0.0, 0.0, 1.0));

        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(0.0, length));
        texcoords.push_back(glm::vec2(0.0, 0.0));


      //back 1
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, -height));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(length, 1.0));
        texcoords.push_back(glm::vec2(length, 0.0));


      //back 2
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, 0));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, -height));
        vertices.push_back(glm::vec3(x1 - eps, y1 - eps, -height));

        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));
        normals.push_back(glm::vec3(-1.0, 0.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(length, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));


        //right 1
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(width, 1.0));
        texcoords.push_back(glm::vec2(width, 0.0));


        //right 2
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, 0));
        vertices.push_back(glm::vec3(x2 + eps, y2 + eps, -height));
        vertices.push_back(glm::vec3(x2 - eps, y2 + eps, -height));

        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));
        normals.push_back(glm::vec3(0.0, 1.0, 0.0));

        texcoords.push_back(glm::vec2(0.0, 1.0));
        texcoords.push_back(glm::vec2(width, 0.0));
        texcoords.push_back(glm::vec2(0.0, 0.0));
    }

}

std::vector<int> Maze::GetBlockedCells(const int cell) const{
    std::vector<int> blockedCells;
    for (auto secondCell : cells[cell]->GetNeighbours()){
        if (findWall(this->walls, std::min(cell, secondCell), std::max(cell, secondCell))!= -1){
            blockedCells.push_back(secondCell);
        }
    }
    return blockedCells;
}
