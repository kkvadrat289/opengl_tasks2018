#pragma once
#include "Maze.hpp"
#include "common/Camera.hpp"
#include <GLFW/glfw3.h>
#include <memory>

class MazeCameraMover : public CameraMover
{
public:
    MazeCameraMover(const std::shared_ptr<Maze> maze);
    ~MazeCameraMover() override {}
    glm::vec3 getPosition();
    glm::vec3 getDirection();
    void handleKey(GLFWwindow* window, int key, int scancode, int action, int mods) override;
    void handleMouseMove(GLFWwindow* window, double xpos, double ypos) override;
    void handleScroll(GLFWwindow* window, double xoffset, double yoffset) override;
    void update(GLFWwindow* window, double dt) override;
    bool CheckCollision(glm::vec2 position);
protected:
    //Положение виртуальный камеры задается в сферических координат
    glm::vec3 _pos;
    glm::quat _rot;

    //Положение курсора мыши на предыдущем кадре
    double _oldXPos;
    double _oldYPos;
    const float MARGIN_WIDTH = 0.4f;

    std::shared_ptr<const Maze> _maze;
};
