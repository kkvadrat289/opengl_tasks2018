#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"
#include "common/Texture.hpp"

#include <dirent.h>
#include <iostream>
#include <vector>
#include <string>
#include "Maze.hpp"
#include "MazeCameraMover.hpp"


class MazeApplication : public Application
{
public:
    void Init(int num, float size){
        maze = std::make_shared<Maze>(num, size);
        maze->GenerateStructure();
    }

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    std::shared_ptr<FreeCameraMover> freeMover;
    std::shared_ptr<MazeCameraMover> mazeMover;

    std::shared_ptr<Maze> maze;

    MeshPtr mazeMesh;
    MeshPtr floorMesh;
    MeshPtr ceilMesh;
    MeshPtr _marker;
    std::vector<MeshPtr> postersMeshVector;
    
    //Координаты источника света
    float _lightRadius = 10.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    float _cutOffAngle = glm::pi<float>() * 0.125f;
    float _attenuation = 0.05f;
    int _lightOn = 1;
    int _observeLightOn = 1;

    glm::vec3 _lightAmbientColor;
    glm::vec3 _lightDiffuseColor;
    glm::vec3 _lightSpecularColor;

    glm::vec3 _observeLightAmbientColor;
    glm::vec3 _observeLightDiffuseColor;
    glm::vec3 _observeLightSpecularColor;

    glm::vec3 _wallAmbientColor;
    glm::vec3 _wallDiffuseColor;
    glm::vec3 _wallSpecularColor;

    float _wallShininess = 20.0f;
    TexturePtr _brickTexture;
    TexturePtr _stoneTexture;
    TexturePtr _brickNormalTexture;
    TexturePtr _stoneNormalTexture;
    TexturePtr _ceilTexture;
    TexturePtr _ceilNormalTexture;

    std::vector<TexturePtr> _posterTextures;
    TexturePtr _posterNormal;

    GLuint _sampler;

    void PrintInfo() const{
        maze->PrintStructure();
    }

    void makeScene() override
    {
        glEnable(GL_CULL_FACE);
        Application::makeScene();

        mazeMover = std::make_shared<MazeCameraMover>(maze);
        freeMover = std::make_shared<FreeCameraMover>();
        _cameraMover = freeMover;

        //Создаем меш с лабиринтом:)
        mazeMesh = maze->makeMesh();
        floorMesh = maze->makeFloor();
        ceilMesh = maze->makeCeil();
        postersMeshVector = maze->makePosters();
        mazeMesh->setModelMatrix(glm::translate( glm::mat4(1.0f), glm::vec3(maze->GetCellNum()*maze->GetCellSize()*0.5f, maze->GetCellNum()*maze->GetCellSize()*0.5f, 0.0f)));

        //Создаем шейдерную программу        

        _shader = std::make_shared<ShaderProgram>("493DetkovaData/AttenuationShader.vert", "493DetkovaData/AttenuationShader.frag");
        _markerShader = std::make_shared<ShaderProgram>("493DetkovaData/marker.vert", "493DetkovaData/marker.frag");
        
        _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
        _lightSpecularColor = glm::vec3(1.0, 1.0, 1.0);

        _observeLightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
        _observeLightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
        _observeLightSpecularColor = glm::vec3(1.0, 1.0, 1.0);

        _wallAmbientColor = glm::vec3(1.0, 1.0, 0.0);
        _wallDiffuseColor = glm::vec3(1.0, 1.0, 0.0);
        _wallSpecularColor = glm::vec3(1.0, 1.0, 1.0);
        _wallShininess = 128.0f;

        _attenuation = 0.05f;

        _marker = makeSphere(0.1f);

        _brickTexture = loadTexture("493DetkovaData/brick.jpg", SRGB::NO);
        _stoneTexture = loadTexture("493DetkovaData/stone.jpg", SRGB::NO);
        _ceilTexture = loadTexture("493DetkovaData/ceil.jpg", SRGB::NO);
        _brickNormalTexture = loadTexture("493DetkovaData/brickNormal.jpg");
        _stoneNormalTexture = loadTexture("493DetkovaData/stoneNormal.jpg");
        _ceilNormalTexture = loadTexture("493DetkovaData/ceilNormal.jpg");
        _posterNormal = loadTexture("493DetkovaData/posterNormal.jpg");

        loadPosterTextures();

        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);
    }

    void loadPosterTextures(){
        DIR *dir;
        struct dirent *ent;
        if ((dir = opendir("./493DetkovaData/posters")) != NULL){
            while ((ent = readdir(dir)) != NULL){
                if (ent->d_name[0] != '.'){
                    _posterTextures.push_back(loadTexture(std::string("493DetkovaData/posters/") + std::string(ent->d_name)));
                }
            }
        }
        closedir(dir);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Maze, Detkova Julia", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            ImGui::Text("LightOn %d", _lightOn);
            ImGui::Text("observationLightOn %d", _observeLightOn);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_observeLightAmbientColor));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_observeLightDiffuseColor));
                ImGui::ColorEdit3("specular", glm::value_ptr(_observeLightSpecularColor));

                ImGui::SliderFloat("radius", &_lightRadius, 0.1f, 20.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
                ImGui::SliderFloat("cutoff", &_cutOffAngle, 0.0f, glm::pi<float>() * 0.5f);
                ImGui::SliderFloat("attenuation", &_attenuation, 0.01f, 10.0f);
            }
        }
        ImGui::End();
    }


    void draw() override
    {
        Application::draw();
        int width, height;

        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        //Устанавливаем шейдер.
        _shader->use();

        glm::vec3 lightPos = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lightRadius;

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setVec3Uniform("light.pos", mazeMover->getPosition());
        _shader->setVec3Uniform("light.La", _lightAmbientColor);
        _shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
        _shader->setVec3Uniform("light.Ls", _lightSpecularColor);

        _shader->setFloatUniform("light.attenuation", _attenuation);
        _shader->setIntUniform("light.onLight", _lightOn);
        _shader->setFloatUniform("angle", _cutOffAngle);
        _shader->setVec3Uniform("lightDirection", mazeMover->getDirection());

        _shader->setVec3Uniform("observeLight.pos", lightPos);
        _shader->setVec3Uniform("observeLight.La", _observeLightAmbientColor);
        _shader->setVec3Uniform("observeLight.Ld", _observeLightDiffuseColor);
        _shader->setVec3Uniform("observeLight.Ls", _observeLightSpecularColor);
        _shader->setFloatUniform("observeLight.attenuation", 0.0);
        _shader->setIntUniform("observeLight.onLight", _observeLightOn);

        glActiveTexture(GL_TEXTURE0);
        glBindSampler(0, _sampler);
        _brickTexture->bind();

        glActiveTexture(GL_TEXTURE1);
        glBindSampler(1, _sampler);
        _brickNormalTexture->bind();

        glActiveTexture(GL_TEXTURE2);
        glBindSampler(2, _sampler);
        _stoneTexture->bind();

        glActiveTexture(GL_TEXTURE3);
        glBindSampler(3, _sampler);
        _stoneNormalTexture->bind();

        glActiveTexture(GL_TEXTURE4);
        glBindSampler(4, _sampler);
        _ceilTexture->bind();

        glActiveTexture(GL_TEXTURE5);
        glBindSampler(5, _sampler);
        _ceilNormalTexture->bind();

        bindPosters();

        _shader->setIntUniform("diffuseTex", 0);
        _shader->setIntUniform("normalTex", 1);

        _shader->setMat4Uniform("modelMatrix", mazeMesh->modelMatrix());
        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * mazeMesh->modelMatrix()))));

        _shader->setVec3Uniform("material.Ka", _wallAmbientColor);
        _shader->setVec3Uniform("material.Kd", _wallDiffuseColor);
        _shader->setVec3Uniform("material.Ks", _wallSpecularColor);
        _shader->setFloatUniform("material.shininess", _wallShininess);

        mazeMesh->draw();

        _shader->setIntUniform("normalTex", 15);
        for (int i = 0; i < 9; i++){
            _shader->setIntUniform( "diffuseTex", 6 + i);
            postersMeshVector[i]->draw();
        }

        _shader->setIntUniform("diffuseTex", 2);
        _shader->setIntUniform("normalTex", 3);

        floorMesh->draw();

        _shader->setIntUniform("diffuseTex", 4);
        _shader->setIntUniform("normalTex", 5);

        ceilMesh->draw();

        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), mazeMover->getPosition()));
            _markerShader->setVec4Uniform("color", glm::vec4(_lightDiffuseColor, 1.0f));
            _marker->draw();
        }
        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), lightPos));
            _markerShader->setVec4Uniform("color", glm::vec4(_observeLightDiffuseColor, 1.0f));
            _marker->draw();
        }

        glBindSampler(0, 0);
        glUseProgram(0);


    }

    void bindPosters(){
        glActiveTexture(GL_TEXTURE6);
        glBindSampler(6, _sampler);
        _posterTextures[0]->bind();

        glActiveTexture(GL_TEXTURE7);
        glBindSampler(7, _sampler);
        _posterTextures[1]->bind();

        glActiveTexture(GL_TEXTURE8);
        glBindSampler(8, _sampler);
        _posterTextures[2]->bind();

        glActiveTexture(GL_TEXTURE9);
        glBindSampler(9, _sampler);
        _posterTextures[3]->bind();

        glActiveTexture(GL_TEXTURE10);
        glBindSampler(10, _sampler);
        _posterTextures[4]->bind();

        glActiveTexture(GL_TEXTURE11);
        glBindSampler(11, _sampler);
        _posterTextures[5]->bind();

        glActiveTexture(GL_TEXTURE12);
        glBindSampler(12, _sampler);
        _posterTextures[6]->bind();

        glActiveTexture(GL_TEXTURE13);
        glBindSampler(13, _sampler);
        _posterTextures[7]->bind();

        glActiveTexture(GL_TEXTURE14);
        glBindSampler(14, _sampler);
        _posterTextures[8]->bind();

        glActiveTexture(GL_TEXTURE15);
        glBindSampler(15, _sampler);
        _posterNormal->bind();
    }

    void handleKey(int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_Q)
                if (_cameraMover == freeMover)
                    _cameraMover = mazeMover;
                else
                    _cameraMover = freeMover;
            } else if (key == GLFW_KEY_O) {
                _observeLightOn = 1 - _observeLightOn;
            } else if (key == GLFW_KEY_I) {
                _lightOn = 1 - _lightOn;
            }
            Application::handleKey(key, scancode, action, mods);
        }


};

int main()
{
    MazeApplication app;
    app.Init(10, 1.0f);
    app.start();

    return 0;
}
